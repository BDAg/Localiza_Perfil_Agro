# Bem vindo ao projeto AgroProspects
Neste projeto irmeos coletar dados de perfis em redes sociais  que estão relacionadas ao agronegócio.

## Requerimentos
- **Repositório**
    - [GitLab](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#git-setup)
- **Web Bots**
    - [Python 3](https://gitlab.com/BDAg/Localiza_Perfil_Agro#python-3)
    - [PyMongo](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#pymongo)
    - [Requests](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#requests)
    - [Beautiful Soup](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#beautiful-soup)
- **Banco de Dados**
    - [Studio 3T](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#studio-3t)
    - [MongoDB Atlas](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#mongodb-atlas)
    - [Conexão MongoDB Atlas e o Studio 3t](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#Conexão-MongoDB-Atlas-e-o-Studio-3t)


- **FrontEnd**
    - [NodeJS](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#nodejs)
    - [Angular 6](https://gitlab.com/BDAg/Localiza_Perfil_Agro/blob/master/README.md#angular-6)


## Git setup
### 1. DOWNLOAD DO GIT NA MAQUINA:
**site oficial:**
https://about.gitlab.com/installation/#ubuntu

**Instalação:**
>sudo apt-get update

>sudo apt-get install -y curl openssh-server ca-certificates

>sudo apt-get install -y postfix

>curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash

>sudo EXTERNAL_URL="http://gitlab.example.com" apt-get install gitlab-ee

>sudo apt install git

### 2. CONFIGURAÇÃO NA MAQUINA:

>git config --global user.name "Meu nome"

>git config --global user.email "emaildapessoa@email.com"

>ssh-keygen -t rsa -b 4096 -C "emaildapessoa@email.com" ( esse vai gerar sua chave em ~/.ssh)

>cat ~/.ssh/id_rsa.pub (vai trazer um código, copia tudo)
(salva a nova chave lá no gitlab)

### 3.A) CRIAR A PARTIR DE UM PROJETO (PASTA) EXISTENTE:

>cd <caminho_da_pasta_onde_quer_incluir_seu_repositorio>

>git clone git@gitlab.com:grupo/nome_do_projeto.git

### 3.B) CRIAR DE REPOSITORIO NOVO:

>cd existing_folder

>git init

>git remote add origin (caminho ssh do repositório remoto, exemplo: git@gitlab.com:grupo/nome_do_projeto.git)

>git add .

>git commit -m "commit inicial"

>git push origin master

## Git comandos
- **Upload**
	>git add .
	
    >git commit -m "mensagem"
	
    >git push origin master

- **Download**
	>git pull origin master
	
    >git clone 'link do site''

## Python 3
**site oficial:**
https://www.python.org/about/gettingstarted/

## PyMongo
**site oficial:**
http://api.mongodb.com/python/current/

**Instalação:**
>sudo python3 -m pip install pymongo

## Requests
**site oficial:**
http://docs.python-requests.org/en/master/

**Instalação:**
>sudo apt install python3-pip

>pip3 install --upgrade pip

>pip3 install requests

## Beautiful Soup
**site oficial:**
https://www.crummy.com/software/BeautifulSoup/bs4/doc/

**Instalação:**
>sudo apt-get  install  python3-bs4

ou

>easy_install3 beautifulsoup4

ou

>pip3 install  beautifulsoup4

## Banco de dados 
### Studio 3T

**site oficial:**
https://studio3t.com

**Instalação:**
 **Fora do terminal:**
>Primeiramente, baixe o arquivo pelo site (você deverá colocar o seu e-mail e na área escrito "Industry" coloque estudante.

**No terminal (64-bits):**
>tar -xvzf studio-3t-linux-x64.tar.gz

>./studio-3t-linux-x64.sh

**No terminal (32-bits):**
> tar -xvzf studio-3t-linux-x86.tar.gz  

> ./studio-3t-linux-x86.sh

### MongoDB Atlas

**Site oficial:**
https://www.mongodb.com/cloud/atlas
>Descer na página até o botão "Get started for free";
>
>Clicar nele para iniciar a criação de contas, colocando informações como e-mail, primeiro e ultimo nomes e uma senha;
>
>Após criar a conta, irá ser aberta uma área de criação de cluster's, pode ser cancelada;

### Conexão MongoDB Atlas e o Studio 3t
**Conectar no servidor**
>Passar o e-mail utilizado na criação da conta para os adm's do servidor;

**No site do servidor**
https://www.mongodb.com/cloud/atlas
>Após realizar o login na conta deve conectar na cluster do grupo AgroProspects;
>Na cluster, clicar em "Connect";
>Clicar na aba "Connect your aplication";
>Clicar na aba "Connect your aplication";
> Clicar em "I'm using driver 3.6 or later";
> Copiar a linha de programa que aparece;

**No Studio 3t**
>Ir em "Connect";
>Ir em "New Connection";
>Clicar em "From URI";
>Colar a linha de código alterando o escrito "password" e os sinais de maior e menor do lado para a nossa senha do projeto;

## NodeJS

**site oficial:**
https://nodejs.org/en/

**Instalação:**
>sudo apt-get update

>sudo apt-get install nodejs

>sudo apt-get install npm

>curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

>sudo apt-get install -y nodejs

>sudo apt-get install -y build-essential

## Angular 6

**site oficial:**
https://angular.io/

**Instalação:**
>sudo npm install -g @angular/cli

**Criar Projeto novo:**
>ng new my-app

**Iniciar o projeto:**
>cd my-app

>ng serve --open


    Link do Projeto Concluido

>https://agroprospects-bigdata.herokuapp.com/#/dashboard



