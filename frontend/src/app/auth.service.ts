import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private registerUrl = "http://localhost:3000/api/register";
  private loginUrl = "http://localhost:3000/api/login";
  private usuarios = "http://localhost:3000/api/usuarios";
  private deleteUsuarios = "http://localhost:3000/api/delete";
  private updateUsuarios = "http://localhost:3000/api/update";
  private mudarSenha = "http://localhost:3000/api/alterarsenha";

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

  // /////////////////////////////////////////////////////////////////////
  // Buscar informações/dados
  // /////////////////////////////////////////////////////////////////////

  registerUser(user) {
    return this.http.post<any>(this.registerUrl, user)
  }

  loginUser(user) {
    return this.http.post<any>(this.loginUrl, user)
  }

  getUsuarios() {
    return this.http.get<any>(this.usuarios)
  }

  deleteUsers(user){
    console.log('Testando delete --->> ', user)
    return this.http.post<any>(this.deleteUsuarios, user)
  }

  updateUsers(user){
    console.log('Testando update --->> ', user)
    return this.http.post<any>(this.updateUsuarios, user)
  }

  updateSenha(user){
    console.log('Testando alterar senha --->> ', user)
    return this.http.put<any>(this.mudarSenha, user)
  }



  // /////////////////////////////////////////////////////////////////////
  // Permissões
  // /////////////////////////////////////////////////////////////////////

  loggedIn() {
    return !!localStorage.getItem('token')
  }

  adminIn() {
    return !!localStorage.getItem('admin')
  }

  // /////////////////////////////////////////////////////////////////////
  // Local storage
  // /////////////////////////////////////////////////////////////////////

  logoutUser() {
    localStorage.removeItem('token')
    localStorage.removeItem('admin')
    localStorage.removeItem('user')
    this.router.navigate(['/login'])
  }

  getToken() {
    return localStorage.getItem('token')
  }

}
