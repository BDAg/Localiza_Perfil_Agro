import { Component, OnInit, TemplateRef } from '@angular/core';
import { AuthService } from './auth.service';
import { BsModalService } from 'ngx-bootstrap/modal';
// import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  modalRef: BsModalRef;
  title = 'app';
  user: any;
  id: any;

  constructor(
    private authService: AuthService,
    private modalService: BsModalService,
  ) { 
    
   }

   localStorage(){
    let usuario = JSON.parse(localStorage.getItem('user'))
    this.user = usuario[0]
    this.id = usuario[1]
    console.log('User', this.user)
   }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);

  }

  updateSenha(senhaAntiga, senhaNova, email, id){
    console.log('Update senha --->> ', senhaAntiga, senhaNova, email, id)
    let user = { senhaAntiga: senhaAntiga,  senhaNova: senhaNova, email: email, id: id}
    this.authService.updateSenha(user).subscribe(
      res => {
        console.log('Res', res)
      },
      err => console.log('Erro --->>', err)
    )
  }

  ngOnInit() {
    this.localStorage()
  }

}
