import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { BsModalService } from 'ngx-bootstrap/modal';
// import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  modalRef: BsModalRef;

  public usuarios = [];
  listUser: any;

  userDelete: any;

  constructor(
    private authService: AuthService,
    private router: Router,
    private modalService: BsModalService,
  ) { 
    this.getUsuarios()
  }

  ngOnInit() {

  }

  getUsuarios() {
    this.authService.getUsuarios().subscribe(
      res => {
        // for(let x; x < res.lenght; x=x+1){
        //   this.listUser = x
        //   console.log('hEYY', x)
        //   this.usuarios.push(this.listUser)
        // }
        this.usuarios = res.user
        console.log('Res', res.user)
        console.log('Res', this.usuarios)
      },
      err => console.log('Erro --->>', err)
    )
  }

  deleteUsers(user){
    this.userDelete = user
    console.log('Deletar usuario --->> ', this.userDelete)
    this.authService.deleteUsers(this.userDelete).subscribe(
      res => {
        console.log('Res', res)
      },
      err => console.log('Erro --->>', err)
    )
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  confirmar(admin, id) {
    if(admin == 'Sim'){
      admin = 'true'
    }
    else{
      admin = 'false'
    }
    let user = { 'admin': admin, 'id': id }
    console.log('Admin --->> ', user)
    this.authService.updateUsers(user).subscribe(
      res => {
        console.log('Res', res)
      },
      err => console.log('Erro --->>', err)
    )
  }

}


