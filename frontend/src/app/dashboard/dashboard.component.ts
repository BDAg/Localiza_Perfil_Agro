import { Router } from '@angular/router';
import { DashboardService } from './../dashboard.service';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  dadosDashboard = []

  constructor(
    private dashboardService: DashboardService,
    private router: Router,
  ) { }

  ngOnInit() {

  }

  getDadosDashboard() {
    this.dashboardService.getDashboard().subscribe(
      res => this.dadosDashboard = res,
      err => {
        console.log('Err --->> ', err)
        if(err instanceof HttpErrorResponse) {
          if(err.status === 401){
            this.router.navigate(['/login'])
          }
        }
      }
    )
    console.log('Dados dashboard --->> ', this.dadosDashboard)
  }

}
