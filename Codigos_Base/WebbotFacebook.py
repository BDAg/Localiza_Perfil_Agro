from bs4 import BeautifulSoup
import requests 
import pymongo
from lxml import html
import time
import random
import re
import os
import logging
import datetime
from http.cookiejar import LWPCookieJar

global s, mydb

# CONEXAO COM O BANCO
print('.::CONECTANDO COM O BANCO::.')
conexao = pymongo.MongoClient("mongodb+srv://botFacebook:bc123456!@dadosface-aakyu.mongodb.net/test")
mydb = conexao['agroprospects']

s = requests.Session()

def criaSession(login, senha):
		s.cookies = LWPCookieJar('cookiejar')
		if not os.path.exists('cookiejar'):
			print('.:: Armazenando Cookies ::.')
			s.cookies.save()
			loginFacebook(login, senha)
		else:
			print('.:: Carregando Cookies ::.')
			s.cookies.load(ignore_discard=True)
			req = s.get('https://m.facebook.com/')
			soup = BeautifulSoup(req.content,'html.parser')
			
			textoSoup = str(soup)
			if textoSoup.find("Participe do Facebook") != 0:
				loginFacebook(login, senha)
		s.cookies.save(ignore_discard=True)

def loginFacebook(email,senha):
	vetor1 = []
	vetor2 = []

	req = requests.get('https://m.facebook.com/')
	soup = BeautifulSoup(req.content,'html.parser')

	for x in soup.find_all('input'):
		requisicao = x['name']
		if str(requisicao) == 'lsd' or str(requisicao) == 'li' or str(requisicao) == 'm_ts':
			vetor1.append(requisicao)
			resposta = x['value']
			vetor2.append(resposta)
			
	dicionario = dict(zip(vetor1,vetor2))

	dicionario['email'] = email
	dicionario['pass'] = senha
	dicionario['login'] = 'Log In'

	r = s.post('https://m.facebook.com/login.php?refsrc=https%3A%2F%2Fm.facebook.com%2F&amp;refid=8', data=dicionario)
	print('---------~---------')
	print(" Status: Logado")
	print('---------~---------')
	print(' Procurando pessoas')

	mineraSobre()


def mineraSobre():

	# Aqui coleta os URLs do banco.
	busca_mongo_perfil = mydb.urls_grupo.find({'coletado_info': False},no_cursor_timeout=True)
	for perfil in busca_mongo_perfil:
		
		urlFacebook = str(perfil['urlFacebook'])
		nomeGrupo = str(perfil['nomeGrupo'])
		idFacebook = str(perfil['idFacebook'])


		trabalho = []
		estudo = []

		nascFace = ''
		sexoFace = ''
		cidadeFace = ''

		reqSobre = s.get('https://m.facebook.com/' + urlFacebook)
		
		#Essa parte consegue o nome da pessoa.

		soupNew = BeautifulSoup(reqSobre.content,'html.parser')
		nomeFace = soupNew.title.text


		#Essa parte consegue a cidade que ele morou.

		nivel11 = soupNew.find_all('div',{'title':'Cidade atual'})
		for count in nivel11:
			obj12=BeautifulSoup(str(count),'html.parser')
			cidadeInfo = obj12.select('table tr td div a')
			for x1 in cidadeInfo:
				cidadeFace = str(x1.text)
		
		#Essa parte consegue a data de nascimento
		nivel12 = soupNew.find_all('div',{'title':'Data de nascimento'})
		if(str(nivel12) != ''):    
			for count in nivel12:
				obj22=BeautifulSoup(str(count),'html.parser')
				nascInfo = obj22.select('table tr td div')
				for x2 in nascInfo:
					nascFace = (str(x2.text).replace('Data de nascimento',''))
					pass

		#Essa parte consegue o sexo da pessoa.
		nivel13 = soupNew.find_all('div',{'title':'Gênero'})
		for count in nivel13:
			obj32=BeautifulSoup(str(count),'html.parser')
			sexInfo = obj32.select('table tr td div')
			for x3 in sexInfo:
				sexoFace = (str(x3.text).replace('Gênero',''))

		#Essa parte conegue o(s) local(is) que ela trabalha
		workinfo = soupNew.select('div#work div div div div div div span a')
		for x in workinfo:
			trabalho.append(str(x.text))


		#Essa parte conegue o(s) local(is) que ela estuda
		studyinfo = soupNew.select('div#education div div div div div div div span a')
		for x in studyinfo:
			estudo.append(str(x.text))

		
		print('\n==============================================================================================')
		
		print('Nome: '+(nomeFace))

		if(nascFace == ''):
			nascFace = 'Indisponível'
			print('data de nascimento: ' + nascFace)
		else:
			print('Data de nascimento: '+(nascFace))

		if(sexoFace == ''):
			sexoFace = 'Indisponível'
			print('Sexo: '+(sexoFace))
		else:
			print('Sexo: '+(sexoFace))
		
		if(cidadeFace == ''):
			cidadeFace = 'Indisponível'
			print('Cidade Atual: '+(cidadeFace))
		else:
			print('Cidade Atual: '+(cidadeFace))
		
		if(workinfo == []):
			trabalho.append('Indisponível')
			print("trabalhou: " + (str(trabalho)))
		else:
			print("trabalhou: " + (str(trabalho)))
		
		if(studyinfo == []):
			estudo.append('Indisponível')
			print("Estudou: " + (str(estudo)))
		else:
			print("Estudou: " + (str(estudo)))

		print('==============================================================================================\n')

		salva_mongo(nomeFace, nascFace, sexoFace, cidadeFace, trabalho, estudo, nomeGrupo, idFacebook)

		timeRandom = random.randint(8, 20)
		time.sleep(timeRandom)

		


def salva_mongo(nome, data, sexo, cidade, trabalho, estudo, nomeGrupo, idFacebook):

	mydb.urls_grupo.update_one(
		{
			"idFacebook" : str(idFacebook)
		},
		{
			"$set" : {
			"coletado_info" : True
		}},
		upsert=False
	)

	mydb.dados_Perfil.insert_one(

		dict({
			"nomeGrupo" : nomeGrupo,
			"idFacebook" : idFacebook,
			"nome" : nome,
			"data_nasc" : data,
			"sexo" : sexo,
			"cidade" : cidade,
			"trabalho" : trabalho,
			"estudo" : estudo,
			}),

	)

	print('SALVANDO NO MONGO...')


criaSession('teodorojose186@gmail.com','01@@01jfa')

