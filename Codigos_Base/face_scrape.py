from bs4 import BeautifulSoup
import requests 
# import pymongo
from lxml import html
import time
import random
import re
import os
import logging
import datetime
from http.cookiejar import LWPCookieJar

global s

s = requests.Session()

def criaSession(login, senha):
		s.cookies = LWPCookieJar('cookiejar')
		if not os.path.exists('cookiejar'):
			print('.:: Armazenando Cookies ::.')
			s.cookies.save()
			loginFacebook(login, senha)
		else:
			print('.:: Carregando Cookies ::.')
			s.cookies.load(ignore_discard=True)
			req = s.get('https://m.facebook.com/')
			soup = BeautifulSoup(req.content,'html.parser')
			
			textoSoup = str(soup)
			if textoSoup.find("Participe do Facebook") != 0:
				loginFacebook(login, senha)
		s.cookies.save(ignore_discard=True)

def loginFacebook(email,senha):
	vetor1 = []
	vetor2 = []

	req = requests.get('https://m.facebook.com/')
	soup = BeautifulSoup(req.content,'html.parser')

	for x in soup.find_all('input'):
		requisicao = x['name']
		if str(requisicao) == 'lsd' or str(requisicao) == 'li' or str(requisicao) == 'm_ts':
			vetor1.append(requisicao)
			resposta = x['value']
			vetor2.append(resposta)
			
	dicionario = dict(zip(vetor1,vetor2))

	dicionario['email'] = email
	dicionario['pass'] = senha
	dicionario['login'] = 'Log In'

	r = s.post('https://m.facebook.com/login.php?refsrc=https%3A%2F%2Fm.facebook.com%2F&amp;refid=8', data=dicionario)
	timeRandom = random.randint(8, 20)
	time.sleep(timeRandom)
	mineracao()

def mineracao():

	firstReq = s.get('https://m.facebook.com/search/108807442476517/residents/present/intersect/')

	soupNew = BeautifulSoup(firstReq.content,'html.parser')
	#print(soupNew)
	nomeFacebook = soupNew.select('div#BrowseResultsContainer div div div table tbody tr td div div')
	for x in nomeFacebook:
		print (str(x.text).replace('\n',''))


criaSession('email@email.com','*********')