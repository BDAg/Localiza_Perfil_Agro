
# Neste codigo serao usados conceitos basicos de web bot, usando as bibliotecas do Python (REQUESTS E BEATIFUL SOUP).

##################################################
# Parte de definicao de bibliotecas a serem usadas
import requests
from bs4 import BeautifulSoup
import csv
###################################################

# declaracao de variavel, que sera usada como contador
count = 1


# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

# looping que ira entrar em todas as paginas do site
while count < 389:
    # requisicao do site com a incrementacao do contador das paginas, usando o requests
    page = requests.get('http://agronomos.ning.com/profiles/friend/list?page=%d' % count)
    # busca os recursos html do site
    contents = page.content
    # organizacao para uma melhor visualizacao do html do site, usando beautiful soup
    soup = BeautifulSoup(contents, 'lxml')
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


##########################################################
    # lopping que vai buscar todos os membros do site, usando beautiful soup
    for member in soup.find_all('div', class_='member_item_detail'):
        # encontra os nomes dos membros
        nome = member.h5.a.text
        # encontra as cidades as quias os membros moram
        cidade = member.p.text
        # mostra esses dados na tela
        print (nome + " -> " + cidade)
    # ao fim de cada pagina ele encrementa mais um as contador, para mudar de pagina
    count = count + 1
#############################################################
