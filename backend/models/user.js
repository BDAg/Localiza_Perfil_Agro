const mongoose = require('mongoose')

const Schema = mongoose.Schema
const userSchema = new Schema({
    email: String,
    password: String,
    admin: String
})

const prospectsSchema = new Schema({
    nome: String,
})

module.exports = mongoose.model('user', userSchema, 'users')
module.exports = mongoose.model('prospect', prospectsSchema, 'dados_Perfil')